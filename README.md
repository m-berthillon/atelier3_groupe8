# groupe8-atelier3
Groupe 8 :
Lucas MIGLIARINI, Mickaël BERTHILLON, Oscar SAPY, Thibaut GOULLET

Rattrapage : Mickaël BERTHILLON 

Analyse Sonarqube, tests unitaires et proxy nginx.



microservices vs SOA

L'architecture en microservices se caractérise par des services fins et spécialisés, favorisant un faible couplage et une haute cohésion, ce qui permet une grande indépendance entre les services. 
Cette approche facilite la communication via des protocoles légers et la gestion autonome des bases de données par chaque service, offrant ainsi une flexibilité technologique et une facilité de déploiement et d'évolutivité.

Contrairement aux microservices, l'architecture SOA (Service Oriented Architecture) s'oriente vers des services de plus grande granularité, centrés sur les processus d'affaires et pouvant présenter un couplage plus important entre eux, ce qui peut compliquer les mises à jour.
SOA utilise souvent un bus de service d'entreprise pour la communication, risquant un point de défaillance unique, et tend à uniformiser la technologie, contrairement à la diversité technologique des microservices.