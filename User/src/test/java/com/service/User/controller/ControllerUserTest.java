package com.service.User.controller;

import com.library.DTOUser.DTO.DTOUser;
import com.service.User.modele.Users;
import com.service.User.service.ServiceUser;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;


//@WebMvcTest(ControllerUser.class)
//@BootstrapWith
@AutoConfigureMockMvc
@SpringBootTest
public class ControllerUserTest {


    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private ControllerUser userController;

    @MockBean
    private ServiceUser serviceUser;

    @Test
    public void testAddUser() throws Exception {
        String requestBody = "{\"username\": \"testUser\", \"password\": \"password\"}";
        mockMvc.perform(MockMvcRequestBuilders.post("/users/")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }




    @Test
    public void testGetUser() throws Exception {
        String username = "law";
        Users expectedUser = new Users(3,"ww. Luffy", "law", "$2a$12$CvTE8HN4/okt0e/lP2jqPuXLtIykxz9gw5XBVIk3uF3SSCkCnUFV2","user",1234);
        doReturn(expectedUser).when(serviceUser).getUserNoPwd(username);
/*
        mockMvc.perform(MockMvcRequestBuilders.get("/users/me/{username}", username)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());*/
                //.andExpect(MockMvcResultMatchers.jsonPath("").value(expectedUser.getUsername())); // Adjust this according to your JSON structure
    }



}