package com.service.User;

import com.library.DTOUser.DTO.DTOUser;
import com.service.User.controller.ControllerUser;
import com.service.User.modele.Users;
import com.service.User.service.ServiceUser;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;


@SpringBootTest
public class ControllerUserTest1 {


    @Mock
    private ServiceUser serviceUser;

    @InjectMocks
    private ControllerUser userController;

    @Test
    public void testAddUser() {
        // Arrange
        DTOUser userDto = new DTOUser();

        // Act
        ResponseEntity<?> responseEntity = userController.addUser(userDto);

        // Assert
        assertEquals(ResponseEntity.ok("user added"), responseEntity);
        verify(serviceUser).addUser(userDto);
    }

    @Test
    public void testGetUser() {
        // Arrange
        String username = "testUser";
        Users expectedUser = new Users();
        doReturn(expectedUser).when(serviceUser).getUserNoPwd(username);

        // Act
        Users result = userController.getUser(username);

        // Assert
        assertEquals(expectedUser, result);
    }

    @Test
    public void testGetAllUser() {
        // Arrange
        List<Users> expectedUsers = new ArrayList<>();
        // Add some users to the list
        doReturn(expectedUsers).when(serviceUser).getAllUserNoPwd();

        // Act
        List<Users> result = userController.getAllUser();

        // Assert
        assertEquals(expectedUsers, result);
    }
}
