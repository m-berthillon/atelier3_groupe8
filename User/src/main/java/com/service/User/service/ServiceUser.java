package com.service.User.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import org.springframework.web.client.RestTemplate;

import com.library.DTOUser.DTO.DTOUser;

import com.service.User.modele.Users;
import com.service.User.repo.RepoUser;


@Service
public class ServiceUser {
	
	private final RepoUser uRepo;
	
	@Value("${card.getcard}")
	private String cardgetcard ;

	ServiceUser(RepoUser uRepo) {
		this.uRepo=uRepo;
	}
	
	public DTOUser getUser(int id) {
		var optionalUsers = uRepo.findById(id);
		Users Users = optionalUsers.get();
		if(Users != null)
		{
			return createDTO(Users);
		}
		return null;
	}
	
	public List<Integer> getCardIdsToUser(int id)
	{
		return uRepo.findCardIDByIdUser(id);
	}
	
	public DTOUser addCardIdsToUser(int UsersId, List<Integer> cardIds) {
	    Users Users = uRepo.findById(UsersId).orElse(null);
	    if (Users == null) {
	        return null;
	    }
	    List<Integer> availableCardIds = getCardId(cardIds);
	    
	    List<Integer> cardsUsers = Users.getCardID();
	    
	    for (Integer cardId : availableCardIds) {
	        if (!cardsUsers.contains(cardId)) {
	            cardsUsers.add(cardId);
	        }
	    }

	    Users.setCardID(cardsUsers);
	    uRepo.save(Users);
	    return createDTO(Users);
	}
	
	public DTOUser addCardIdToUser(int UsersId, Integer cardId) {
	    Users Users = uRepo.findById(UsersId).orElse(null);
	    if (Users == null) {
	        return null;
	    }
    
	    List<Integer> cardsUsers = Users.getCardID();
	    

        if (!cardsUsers.contains(cardId)) {
            cardsUsers.add(cardId);
        }
	    

	    Users.setCardID(cardsUsers);
	    uRepo.save(Users);
	    return createDTO(Users);
	}

	
	public List<Integer> getCardId(List<Integer> cardIds) {
	    String cardServiceUrl = "http://localhost:8081/cards"; 
	    RestTemplate restTemplate = new RestTemplate();
	    
	    // create a wrapper object that contains the list of cards as a field
	    Map<String, List<Integer>> requestBody = new HashMap<>();
	    requestBody.put("cardIds", cardIds);
	    
	    // pass the wrapper object as the request body
	    ResponseEntity<List> response = restTemplate.postForEntity(cardgetcard, requestBody, List.class);
	    
	    List<Integer> result = response.getBody();
	    
	    return result;
	}
	
	public DTOUser removeCardIdsToUser(int UsersId, Integer cardId) {
	    Users Users = uRepo.findById(UsersId).orElse(null);
	    if (Users == null) {
	        return null;
	    }
    
	    List<Integer> cardsUsers = Users.getCardID();
	    

        if (cardsUsers.contains(cardId)) {
            cardsUsers.remove(cardId);
        }
	    

	    Users.setCardID(cardsUsers);
	    uRepo.save(Users);
	    return createDTO(Users);
	}
	
	
	public DTOUser createDTO (Users  Users)
	{
		DTOUser  dtoUsers = new DTOUser();
		dtoUsers.setId(Users.getId());
		dtoUsers.setMoney(Users.getMoney());
		dtoUsers.setName(Users.getName());
		dtoUsers.setUsername(Users.getUsername());
		dtoUsers.setCardId(Users.getCardID());
		return dtoUsers ;
	}

	public void updateMoney(DTOUser dtoUsers) {
	    Users Users = uRepo.findById(dtoUsers.getId()).orElse(null);
	    if (Users != null) {
	        Users.setMoney(dtoUsers.getMoney());
	        uRepo.save(Users);
	    }
	}
	
	/*
	public boolean verifyUserssPassword(String name, String password) {
		Userss isPasswordCorrect = uRepo.verifyUserssPassword(name, password);
		if(isPasswordCorrect != null) {
			return true;
		}else {
			return false;
	}
	    }
	
	public boolean VerifyUserssExiste(String name, String surname) {
		Userss isUserssExist = uRepo.VerifyUserssExiste(name, surname);
		if(isUserssExist != null) {
			return true;
		}else {
		return false;
	}
	}
	
	public boolean createUserss(Userss Users) {
		if(!VerifyUserssExiste(Users.getName(), Users.getSurname())) {
			uRepo.save(Users);
			return true;
		}
		return false;
	}
	
	public Userss connectUsers(Userss Users) {
		if (verifyUserssPassword(Users.getName(), Users.getPassword()) == true){
			return uRepo.verifyUserssPassword(Users.getName(), Users.getPassword());
		}
		return null;
	}*/
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public Optional<Users>  addUser(DTOUser DTOUsers) {
		Optional<Users> u =uRepo.findUserByUsername(DTOUsers.getUsername());
		if( !u.isPresent()){
			Users u_new=new Users();
			u_new.setUsername(DTOUsers.getUsername());
			u_new.setPassword(passwordEncoder.encode(DTOUsers.getPassword()));
//			uRepo.save(u_new);
			return Optional.of(uRepo.save(u_new));
		}
		return u;
	}
	
	public boolean setUser(DTOUser DTOUsers, String Usersname) {
		Optional<Users> uOpt =uRepo.findUserByUsername(Usersname);
		if( uOpt.isPresent()){
			Users u=uOpt.get();
			u.setUsername(DTOUsers.getUsername());
			u.setPassword(passwordEncoder.encode(DTOUsers.getPassword()));
			uRepo.save(u);
			return true;
		}
		return false;
	}
	
	public boolean delUser(String Usersname) {
		Optional<Users> u =uRepo.findUserByUsername(Usersname);
		if( u.isPresent()){
			uRepo.delete(u.get());
			return true;
		}
		return false;
	}
	
	public Users getUser(String Usersname) {
		Optional<Users> u =uRepo.findUserByUsername(Usersname);
		if( u.isPresent()){
			return u.get();
		}
		return null;
	}
	
	public Users getUserNoPwd(String Usersname) {
		Optional<Users> uOpt =uRepo.findUserByUsername(Usersname);
		if( uOpt.isPresent()){
			Users u=uOpt.get();
			u.setPassword("*************");
			return u;
		}
		return null;
	}

	public List<Users> getAllUserNoPwd() {
		List<Users> UsersList = new ArrayList<>();
		uRepo.findAll().forEach(s -> {
			s.setPassword("*************");
			UsersList.add(s);
		});
		return UsersList;
	}
	
}

