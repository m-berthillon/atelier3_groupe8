package com.service.User.config.security;

import com.service.User.auth.controller.AppAuthProvider;
import com.service.User.auth.controller.AuthService;
import com.service.User.auth.controller.JwtAuthEntryPoint;
//import com.service.User.auth.controller.JwtRequestFilter;
import com.service.User.auth.controller.JwtRequestFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.beans.Customizer;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    private final AuthService authService;
    private final JwtAuthEntryPoint jwtAuthenticationEntryPoint;
	private final JwtRequestFilter jwtRequestFilter;
	private final PasswordEncoder passwordEncoder;
	public SecurityConfig( AuthService authService, 
							JwtAuthEntryPoint jwtAuthenticationEntryPoint,
							JwtRequestFilter jwtRequestFilter,
							PasswordEncoder passwordEncoder) {
		this.authService=authService;
		this.jwtAuthenticationEntryPoint=jwtAuthenticationEntryPoint;
		this.jwtRequestFilter=jwtRequestFilter;
		this.passwordEncoder=passwordEncoder;
	}

	// Security chain for JWT
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		// use to allow direct login call without hidden value csfr (Cross Site Request
		// Forgery) needed
		 http.csrf(csrf->csrf.disable())
				.cors(cors ->cors.disable())
				 .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
				.authorizeHttpRequests(auth ->
				auth
						.requestMatchers("http://localhost:8082/**")
						.permitAll()
						.requestMatchers("/users/**","/login","/h2-console/**")
						.permitAll()
						.requestMatchers(HttpMethod.POST,"/login")
						.permitAll()
						.anyRequest().authenticated()
				)
				//.exceptionHandling()
				//.authenticationEntryPoint(jwtAuthenticationEntryPoint)
				//.and()

				.authenticationProvider(getProvider());
				//.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
		return http.build();
	}

	@Bean
	public AuthenticationProvider getProvider() {
		AppAuthProvider provider = new AppAuthProvider();
		provider.setUserDetailsService(authService);
		provider.setPasswordEncoder(passwordEncoder);
		return provider;
	}


	// update due to WebSecurityConfigurerAdapter deprecated
	@Bean
	AuthenticationManager authenticationManager(AuthenticationConfiguration authConfiguration) throws Exception {
		return authConfiguration.getAuthenticationManager();
	}
}