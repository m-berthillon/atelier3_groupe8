package com.service.User.auth.controller;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.service.User.modele.Users;


public interface UserRepository extends CrudRepository<Users, Integer> {
    Optional<Users>  findUserByUsername(String username);
}