package com.service.User.repo;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.service.User.modele.Users;


public interface RepoUser extends CrudRepository<Users, Integer>{	
	
	/*@Query("select u from Users u where u.name = :name and u.password = :password")
	Users verifyUsersPassword(@Param("name") String name, @Param("password") String password); // return true si le mdp est bon
	
	@Query("select u from Users u where u.name = :name and u.surname = :surname")
	Users VerifyUsersExiste(@Param("name") String name, @Param("surname") String surname); // return true si le users existe deja*/
	
	Optional<Users>  findUserByUsername(String username);
	
	@Query("select u.CardID from Users u where u.id = :id")
	List<Integer> findCardIDByIdUser(@Param("id") int id); 
}
