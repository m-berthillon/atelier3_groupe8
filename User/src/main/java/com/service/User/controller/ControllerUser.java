package com.service.User.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.library.DTOUser.DTO.*;
import com.service.User.modele.Users;
import com.service.User.service.ServiceUser;
@Controller
@RequestMapping("/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControllerUser {
	@Autowired
	ServiceUser serviceUser;
	

	
	@RequestMapping(method=RequestMethod.GET,value="/{id}/card_id")
	public ResponseEntity<List<Integer>> getIdsToUser(@PathVariable int id) {
	    return
				ResponseEntity.ok(serviceUser.getCardIdsToUser(id));
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/{id}/card_id")
	public DTOUser addIdsToUser(@PathVariable int id, @RequestBody List<Integer> CardId) {
	    return serviceUser.addCardIdsToUser(id, CardId);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/{id}/{card_id}")
	public DTOUser deleteIdsToUser(@PathVariable int id, @PathVariable int card_id) {
	    return serviceUser.removeCardIdsToUser(id, card_id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/{id}/{card_id}")
	public DTOUser addIdToUser(@PathVariable int id, @PathVariable int card_id) {
	    return serviceUser.addCardIdToUser(id, card_id);
	}
	

	
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/{money}")
	public DTOUser updateUserMoney(@PathVariable int id, @PathVariable int money) {
		DTOUser dtoUser = serviceUser.getUser(id);
	    if (dtoUser != null) {
	        dtoUser.setMoney(money);
	        serviceUser.updateMoney(dtoUser);
	        return dtoUser;
	    }
	    return null;
	}
	
	/*@RequestMapping(method=RequestMethod.POST,value="/inscription")
	public DTOUser redirectRegister(@RequestBody Users user) {
		if (serviceUser.createUsers(user) == true) {
			return serviceUser.createDTO(user);	
		}
		return null;
	}
	
	
	@RequestMapping(method=RequestMethod.DELETE,value="/user/{id}")
	public DTOUser removeIdsToUser(@PathVariable int id, @RequestBody List<Integer> CardId) {
	    return serviceUser.removeCardIdsToUser(id, CardId);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/connexion")
	public Users redirectLogin(@RequestBody Users user) {
		if(serviceUser.connectUser(user) != null) {
			return serviceUser.connectUser(user);	
		}
		
		return null;
	}*/

	
	@RequestMapping(method=RequestMethod.POST,value="/")
	public ResponseEntity<?> addUser(@RequestBody DTOUser userDto) {
		 serviceUser.addUser(userDto);
		 return ResponseEntity.ok("user added");
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/{username}")
	public Users getUser(@PathVariable String username) {
		return serviceUser.getUserNoPwd(username);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/{username}")
	public boolean getUser(@PathVariable String username, @RequestBody DTOUser userDto) {
		return serviceUser.setUser(userDto,username);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/{username}")
	public boolean delUser(@PathVariable String username) {
		return serviceUser.delUser(username);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/")
	public List<Users> getAllUser() {
		return serviceUser.getAllUserNoPwd();
	}

	
}
