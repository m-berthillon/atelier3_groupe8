INSERT INTO USERS (id, name, username, password,role,money) VALUES (1,'Monkey D. Luffy', 'luf', '$2a$10$dq/6L9dKOq1O.Tlmm16tdeNJ0VBH6cfWE2I6MbRIHX2EQZsRV39f.','admins',1234);
INSERT INTO USERS (id, name, username, password,role,money) VALUES (2,'Monkey D. Luffy', 'man', '$2a$10$dq/6L9dKOq1O.Tlmm16tdeNJ0VBH6cfWE2I6MbRIHX2EQZsRV39f.','admins',1234);
INSERT INTO USERS (id, name, username, password,role,money) VALUES (3,'ww. Luffy', 'mike', '$2a$12$CvTE8HN4/okt0e/lP2jqPuXLtIykxz9gw5XBVIk3uF3SSCkCnUFV2','user',1234);

