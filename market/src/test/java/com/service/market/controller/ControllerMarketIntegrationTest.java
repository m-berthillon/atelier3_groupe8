package com.service.market.controller;


import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.util.ArrayList;
import java.util.List;

import com.library.DTOCard.DTO.DTOCard;
import com.service.market.entity.Market;
import com.service.market.service.ServiceMarket;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(ControllerMarket.class)
@AutoConfigureMockMvc
public class ControllerMarketIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ServiceMarket mService;

    @Test
    public void testBuy() throws Exception {
        int idCard = 1;
        String idbuyer = "buyerId";
        String idseller = "sellerId";
        Market expectedMarket = new Market();
        doReturn(expectedMarket).when(mService).buy(idCard, idbuyer, idseller);

        mockMvc.perform(post("/market/buy/{idCard}/{idbuyer}/{idseller}", idCard, idbuyer, idseller)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

    }

    @Test
    public void testSell() throws Exception {
        int idCard = 1;
        Market expectedMarket = new Market();
        doReturn(expectedMarket).when(mService).sell(idCard);

        mockMvc.perform(post("/market/sell/{idCard}", idCard)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

    }

    @Test
    public void testShowBuyerCards() throws Exception {
        int id = 1;
        List<DTOCard> expectedCards = new ArrayList<>();

        doReturn(expectedCards).when(mService).showBuyerCards(id);

        mockMvc.perform(get("/market/showBuyerCards/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

    }

    @Test
    public void testShowSellerCards() throws Exception {
        int id = 1;
        List<DTOCard> expectedCards = new ArrayList<>();

        doReturn(expectedCards).when(mService).showSellerCards(id);

        mockMvc.perform(get("/market/showSellerCards/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

    }
}
