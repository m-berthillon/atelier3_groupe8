package com.service.market.controller;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;


import java.util.ArrayList;
import java.util.List;

import com.library.DTOCard.DTO.DTOCard;
import com.service.market.entity.Market;
import com.service.market.service.ServiceMarket;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class ControllerMarketTest {

    @Mock
    private ServiceMarket mService;

    @InjectMocks
    private ControllerMarket controller;

    @Before
    public void setUp() {
        // Initialize mocks if needed
    }

    @Test
    public void testShowSellerCards() {
        // Mock service method
        List<DTOCard> cards = new ArrayList<>();
        when(mService.showSellerCards(anyInt())).thenReturn(cards);

        // Call controller method
        List<DTOCard> response = controller.showSellerCards(1);

        // Verify service method was called with correct parameters
        verify(mService).showSellerCards(1);

        // Verify response content
        assertEquals(cards, response);
    }
}
