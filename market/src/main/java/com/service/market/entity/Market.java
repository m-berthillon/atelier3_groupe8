package com.service.market.entity;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Table;

@Entity
@Table(name = "MARKET")
public class Market {
	@jakarta.persistence.Id
	@GeneratedValue
	 private Integer Id;
	 private String Type;
	 private Date Date;
	 private Integer CardId;
	 private Integer UserId;


	public Integer getId() {
		return Id;
	}


	public void setId(Integer id) {
		Id = id;
	}


	public String getType() {
		return Type;
	}


	public void setType(String type) {
		Type = type;
	}


	public Date getDate() {
		return Date;
	}


	public void setDate(Date date) {
		Date = date;
	}


	public Integer getCardId() {
		return CardId;
	}


	public void setCardId(Integer cardId) {
		CardId = cardId;
	}


	public Integer getUserId() {
		return UserId;
	}


	public void setUserId(Integer userId) {
		UserId = userId;
	}


	public Market() {}

	
}

