package com.service.market.repository;

import org.springframework.data.repository.CrudRepository;

import com.service.market.entity.Market;

public interface RepoMarket extends CrudRepository<Market,Integer> {
    
}
