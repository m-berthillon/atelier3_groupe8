package com.service.market.controller;

import com.library.DTOCard.DTO.DTOCard;
import com.service.market.entity.Market;
import com.service.market.service.ServiceMarket;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/market")
public class ControllerMarket {

    private final ServiceMarket mService;
	
	public ControllerMarket(ServiceMarket mService) {
		this.mService=mService;
	}

	@RequestMapping(method=RequestMethod.POST,value="/buy/{idCard}/{idbuyer}/{idseller}")
	public Market buy(@PathVariable int idCard,@PathVariable String idbuyer,@PathVariable String idseller){
		return mService.buy(idCard,idbuyer,idseller);
	}

	@RequestMapping(method=RequestMethod.POST,value="/sell/{idCard}")
	public Market sell(@PathVariable int idCard){
		return mService.sell(idCard);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/showBuyerCards/{id}")
	public List<DTOCard> showBuyerCards(@PathVariable Integer id){
		return mService.showBuyerCards(id);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/showSellerCards/{id}")
	public List<DTOCard> showSellerCards(@PathVariable Integer id){
		return mService.showSellerCards(id);
	}
	


}
