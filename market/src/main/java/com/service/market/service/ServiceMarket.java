package com.service.market.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.library.DTOCard.DTO.DTOCard;

import com.library.DTOUser.DTO.DTOUser;
import com.service.market.DTO.DTOMarket;
import com.service.market.entity.Market;
import com.service.market.repository.RepoMarket;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ServiceMarket {
    private final RepoMarket mRepository;


	public ServiceMarket(RepoMarket mRepository) {
        this.mRepository = mRepository;
	}
	
	@Value("${card.getcard}")
	private String cardgetcard ;
	
	@Value("${user.getuser}")
	private String usergetuser ;

	
	
		
	public DTOMarket createDTO (Market market, Integer cardId) {
	    DTOMarket dtoMarket = new DTOMarket();
	    dtoMarket.setId(market.getId());
	    dtoMarket.setType(market.getType());
	    dtoMarket.setDate(market.getDate());
	    dtoMarket.setCardId(cardId); // Supposant que DTOMarket a une méthode setCardId
	    return dtoMarket;
	}

		
	
	public List<Long> getCardIds() {
	    String cardServiceUrl = "http://localhost:8081/cards"; 
	    RestTemplate restTemplate = new RestTemplate();
	    List<Long> cardIds = restTemplate.getForObject(cardServiceUrl, List.class);
	    return cardIds;
	}

	
    public Market sell(int idCard) {

        RestTemplate restTemplate = new RestTemplate();

        
        
        String cardResourceUrl
          = "http://localhost:8081/cards";

        String userResourceUrl
          = "http://localhost:8080/users";

        ResponseEntity<DTOCard> responseCard
          = restTemplate.getForEntity(cardResourceUrl+ "/" + idCard, DTOCard.class);
        
          
        DTOCard card = responseCard.getBody();
        
        card.setSell(true);

        Market m = new Market();
        m.setType("sell");
        m.setDate(new Date());
        m.setCardId(card.getId());
        
        restTemplate.put(cardResourceUrl + "/"+ card.getId()+"/true", List.class);
            
        return mRepository.save(m);

	}

    public Market buy(int idCard,String Namebuyer,String Nameseller) {

        RestTemplate restTemplate = new RestTemplate();

        String cardResourceUrl
        = "http://localhost:8081/cards";

      String userResourceUrl
        = "http://localhost:8080/users";

        ResponseEntity<DTOCard> responseCard
          = restTemplate.getForEntity(cardResourceUrl+ "/"+ idCard, DTOCard.class);

        ResponseEntity<DTOUser> responseUser1
          = restTemplate.getForEntity(userResourceUrl+ "/"+ Namebuyer, DTOUser.class);
        
          
        DTOCard card = responseCard.getBody();
        DTOUser user1 = responseUser1.getBody();

        Market m = new Market();

        m.setType("sell");
        m.setDate(new Date());
        m.setUserId(user1.getId());
        m.setCardId(card.getId());
        
        
        
        

        Integer buyerId = m.getUserId();
        ResponseEntity<DTOUser> responseBuyer
          = restTemplate.getForEntity(userResourceUrl + "/" + Namebuyer, DTOUser.class);
        DTOUser buyer = responseBuyer.getBody();
        
        ResponseEntity<DTOUser> responseSeller
        = restTemplate.getForEntity(userResourceUrl + "/" + Nameseller , DTOUser.class);
        
        DTOUser seller = responseSeller.getBody();
        
        restTemplate.delete(userResourceUrl+ "/"+ seller.getId() + "/" + idCard);
        restTemplate.put(userResourceUrl+ "/"+ seller.getId() + "/" + idCard, int.class);

  //      buyer.getCardId().add(card.getId());


        restTemplate.put(userResourceUrl + "/" + seller.getId(), seller);
        restTemplate.put(userResourceUrl + "/" + buyer.getId(), buyer);
        
        seller.setMoney(seller.getMoney() + card.getPrice());
        buyer.setMoney(buyer.getMoney() - card.getPrice());
        
        //restTemplate.put(cardResourceUrl + "/"+ card.getId()+"/0", List.class);

        return mRepository.save(m);
	}
    
     public List<DTOCard> showSellerCards(Integer userId) {
        RestTemplate restTemplate = new RestTemplate();
        String userResourceUrl = "http://localhost:8080/users/";
        String cardResourceUrl = "http://localhost:8081/cards/sellable";
        
        ParameterizedTypeReference<List<Integer>> responseType = new ParameterizedTypeReference<List<Integer>>() {};

     // Exécution de la requête GET avec ResponseEntity
     ResponseEntity<List<Integer>> responseEntity = restTemplate.exchange(
    		 userResourceUrl + userId + "/card_id",
             HttpMethod.GET,
             null,
             responseType
     );
     List<Integer> integers = responseEntity.getBody();
     
     ParameterizedTypeReference<List<DTOCard>> responseType2 = new ParameterizedTypeReference<List<DTOCard>>() {};

     // Exécution de la requête GET avec ResponseEntity
     ResponseEntity<List<DTOCard>> responseEntity2 = restTemplate.exchange(
    		 cardResourceUrl,
             HttpMethod.GET,
             null,
             responseType2
     );
     List<DTOCard> DTOAllCards = responseEntity2.getBody();
     List<DTOCard> DTOCards = new ArrayList<>();
        for (DTOCard dtoCard : DTOAllCards) {
			if(integers.contains(dtoCard.getId())){
				DTOCards.add(dtoCard);
			}
		}
     
        return DTOCards;
    }
     
   
   
    public List<DTOCard> showBuyerCards(Integer userId) {
    	 RestTemplate restTemplate = new RestTemplate();
         String userResourceUrl = "http://localhost:8080/users/";
         String cardResourceUrl = "http://localhost:8081/cards/notsellable";
         
         ParameterizedTypeReference<List<Integer>> responseType = new ParameterizedTypeReference<List<Integer>>() {};

      // Exécution de la requête GET avec ResponseEntity
      ResponseEntity<List<Integer>> responseEntity = restTemplate.exchange(
     		 userResourceUrl + userId + "/card_id",
              HttpMethod.GET,
              null,
              responseType
      );
      List<Integer> integers = responseEntity.getBody();
      
      ParameterizedTypeReference<List<DTOCard>> responseType2 = new ParameterizedTypeReference<List<DTOCard>>() {};

      // Exécution de la requête GET avec ResponseEntity
      ResponseEntity<List<DTOCard>> responseEntity2 = restTemplate.exchange(
     		 cardResourceUrl,
              HttpMethod.GET,
              null,
              responseType2
      );
      List<DTOCard> DTOAllCards = responseEntity2.getBody();
      List<DTOCard> DTOCards = new ArrayList<>();
         for (DTOCard dtoCard : DTOAllCards) {
 			if(!integers.contains(dtoCard.getId())){
 				DTOCards.add(dtoCard);
 			}
 		}
      
         return DTOCards;
    }
    
    

}
