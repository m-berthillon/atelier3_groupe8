package com.service.Game.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.library.DTOCard.DTO.*;
import com.service.Game.modele.Game;

public class serviceGame {
	
	public List<Integer> getDeckIDCard(int idUser) {
	    String cardServiceUrl = "http://localhost:8080/user/" + idUser + "card_id";
	    RestTemplate restTemplate = new RestTemplate();

	    // Effectue une requête GET pour obtenir la liste d'IDs de cartes à partir du service
	    ResponseEntity<List<Integer>> response = restTemplate.exchange(cardServiceUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<Integer>>() {});

	    List<Integer> cardIds = response.getBody();

	    // Vérifie si la liste d'IDs de cartes n'est pas vide et contient au moins deux IDs
	    if (cardIds != null && cardIds.size() >= 2) {
	        // Sélectionne deux IDs de cartes au hasard
	        List<Integer> shuffledIds = new ArrayList<>(cardIds);
	        Collections.shuffle(shuffledIds, new Random());
	        Integer cardId1 = shuffledIds.get(0);
	        Integer cardId2 = shuffledIds.get(1);

	        // Crée une nouvelle liste appelée deck contenant les deux IDs sélectionnés
	        List<Integer> deck = new ArrayList<>();
	        deck.add(cardId1);
	        deck.add(cardId2);

	        return deck;
	    }

	    // Si la liste d'IDs de cartes est vide ou ne contient pas assez d'IDs, retourne null ou une valeur par défaut selon votre besoin
	    return null;
	}
	
	public List<Integer> getDeckIDCardBot() {
	    String cardServiceUrl = "http://localhost:8081/cards"; // URL pour obtenir la liste globale de toutes les cartes
	    RestTemplate restTemplate = new RestTemplate();

	    // Effectue une requête GET pour obtenir la liste globale de toutes les cartes
	    ResponseEntity<List<Integer>> response = restTemplate.exchange(cardServiceUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<Integer>>() {});

	    List<Integer> allCardIds = response.getBody();

	    // Vérifie si la liste globale de toutes les cartes n'est pas vide et contient au moins deux IDs de cartes
	    if (allCardIds != null && allCardIds.size() >= 2) {
	        // Sélectionne deux IDs de cartes au hasard
	        List<Integer> shuffledIds = new ArrayList<>(allCardIds);
	        Collections.shuffle(shuffledIds, new Random());
	        Integer cardId1 = shuffledIds.get(0);
	        Integer cardId2 = shuffledIds.get(1);

	        // Crée une nouvelle liste appelée deck contenant les deux IDs sélectionnés
	        List<Integer> deck = new ArrayList<>();
	        deck.add(cardId1);
	        deck.add(cardId2);

	        return deck;
	    }

	    return null;
	}
	
	public List<DTOCard> idToCard(List<Integer> cardIds) {
	    if (cardIds == null || cardIds.isEmpty()) {
	        return null;
	    }

	    String cardServiceUrl = "http://localhost:8081/card/";
	    RestTemplate restTemplate = new RestTemplate();
	    List<DTOCard> cards = new ArrayList<>();

	    for (Integer cardId : cardIds) {
	        ResponseEntity<DTOCard> response = restTemplate.getForEntity(cardServiceUrl + cardId, DTOCard.class);
	        DTOCard card = response.getBody();

	        if (card != null) {
	            cards.add(card);
	        }
	    }

	    return cards;
	}
	
	public Integer playTurn(Game newGame) {
	    // Récupère les decks du joueur et du bot à partir de l'objet Game
	    List<DTOCard> playerDeck = newGame.getDeckUser();
	    List<DTOCard> botDeck = newGame.getDeckBot();

	    // Vérifie si l'un des decks est vide
	    if (playerDeck.isEmpty()) {
	        newGame.setWinnerID(botDeck.get(0).getId()); // Met l'ID du bot comme gagnant si le deck du joueur est vide
	        return botDeck.get(0).getId(); 
	    }
	    
	    if (botDeck.isEmpty()) {
	        newGame.setWinnerID(playerDeck.get(0).getId()); // Met l'ID du joueur comme gagnant si le deck du bot est vide
	        return playerDeck.get(0).getId();
	    }

	    // Prend la première carte de chaque deck
	    DTOCard playerCard = playerDeck.remove(0);
	    DTOCard botCard = botDeck.remove(0);

	    // Calcul les HP restants pour chaque carte
	    int playerRemainingHP = playerCard.getHp() - botCard.getEnergy();
	    int botRemainingHP = botCard.getHp() - playerCard.getEnergy();

	    // Détermine le gagnant et ajoute les deux cartes à la fin de son deck
	    if (playerRemainingHP >= botRemainingHP) {
	        // Le joueur gagne
	        playerDeck.add(playerCard);
	        playerDeck.add(botCard);
	    } else {
	        // Le bot gagne
	        botDeck.add(botCard);
	        botDeck.add(playerCard);
	    }

	    // Si personne n'a encore gagné, retourne null
	    return null;
	}
	
	public Integer startGame(Game newGame) {
	    // Utilisez les méthodes fournies pour récupérer les identifiants de cartes et les convertir en objets DTOCard
	    List<Integer> playerCardIds = getDeckIDCard(newGame.getId());
	    newGame.setDeckUser(idToCard(playerCardIds));
	    
	    List<Integer> botCardIds = getDeckIDCardBot();
	    newGame.setDeckBot(idToCard(botCardIds));

	    // Continue à jouer des tours jusqu'à ce qu'un gagnant soit déterminé
	    Integer winnerId;
	    do {
	        winnerId = playTurn(newGame);
	    } while (winnerId == null);

	    // Définit l'ID du gagnant dans l'objet Game et renvoie l'ID du gagnant
	    newGame.setWinnerID(winnerId);
	    return winnerId;
	}
}
