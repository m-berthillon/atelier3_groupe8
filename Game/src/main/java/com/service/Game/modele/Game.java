package com.service.Game.modele;

import java.util.List;
import com.library.DTOCard.DTO.*;

public class Game {

	int id;
	int winnerID;
	List<DTOCard> DeckUser;
	List<DTOCard> DeckBot;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getWinnerID() {
		return winnerID;
	}
	public void setWinnerID(int winnerID) {
		this.winnerID = winnerID;
	}
	public List<DTOCard> getDeckUser() {
		return DeckUser;
	}
	public void setDeckUser(List<DTOCard> deckUser) {
		DeckUser = deckUser;
	}
	public List<DTOCard> getDeckBot() {
		return DeckBot;
	}
	public void setDeckBot(List<DTOCard> deckBot) {
		DeckBot = deckBot;
	}
	
}
