package com.service.Game.controller;



import com.library.*;


import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
public class ControllerGame {
    private final ServiceMarket mService;

    public ControllerGame(ServiceMarket mService) {
        this.mService = mService;
    }

    @PostMapping("/transactions/buy")
    public Market buy(@RequestBody DTOMarket transactionDetails) {
        return mService.buy(transactionDetails);
    }

    @PostMapping("/transactions/sell")
    public Market sell(@RequestBody DTOMarket transactionDetails) {
        return mService.sell(transactionDetails);
    }

    @GetMapping("/cards/buyer/{id}")
    public List<DTOMarket> showBuyerCards(@PathVariable Integer id) {
        return mService.showBuyerCards(id);
    }

    @GetMapping("/cards/seller/{id}")
    public List<DTOMarket> showSellerCards(@PathVariable Integer id) {
        return mService.showSellerCards(id);
    }
}
