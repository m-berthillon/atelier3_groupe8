document.getElementById('loginForm').addEventListener('submit', function(event) {
  event.preventDefault();
  var formData = new FormData(this);
  var username = formData.get('username');
  var password = formData.get('password');

  fetch('http://localhost:8080/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ username: username, password: password })
  })
  .then(response => {
    if (!response.ok) {
      throw new Error('Invalid username or password');
    }
    return response.json();
  })
  .then(data => {
    // Handle successful login
    console.log('Token:', data.token);
    localStorage.setItem("token",data.token)
    
    window.location.replace("buycard.html")
  })
  .catch(error => {
    document.getElementById('errorMessage').textContent = error.message;
  });
});
