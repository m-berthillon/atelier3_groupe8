$(document).ready(function () {
    const userId = getCookie("id");
    var money = document.getElementById("money");
    var pseudo = document.getElementById("pseudo");
    const user = getUser(userId).then(user => {
        money.innerHTML = "5000€";
        pseudo.innerHTML = "Test";
    })
  

    $.ajax({
      //url: "http://localhost:8082/market/showSellerCards/3",
      url: "http://localhost:8081/cards/all",
      dataType: "json",
      success: function(data) {
        const tableBody = $('#tableau tbody');
        console.log(data);
        $.each(data, function(index, entry) {
          const row = $('<tr></tr>');
          row.append($('<td></td>').text(entry.id));
          row.append($('<td></td>').text(entry.name));
          row.append($('<td></td>').text(entry.description));
          row.append($('<td></td>').text(entry.family));
          row.append($('<td></td>').text(entry.affinity));
          row.append($('<td></td>').text(entry.energy));
          row.append($('<td></td>').text(entry.hp));
          row.append($('<td></td>').text(entry.price + "€"));
          const button = $('<button>Vendre</button>');
          button.click(function() {
            sellCard(entry.id);
          });
          row.append($('<td></td>').append(button));
          tableBody.append(row);
        });     
        
      }
  });
});


function sellCard(id){
    $.ajax({
        type: "POST",
        url: "market/showSellerCards/" + id,
        dataType: "json",
        success: function(data) {
            location.reload();          
        }
    });
}


function getUser(id) {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: "GET",
            url: "user/user/" + id,
            dataType: "json",
            success: function(data) {
                resolve(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                reject(errorThrown);
            }
        });
    });
}


function getCookie(name) {
    const cookieValue = document.cookie.match('(^|[^;]+)\\s*' + name + '\\s*=\\s*([^;]+)');
    return cookieValue ? cookieValue.pop() : '';
  }