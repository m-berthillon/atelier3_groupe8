
$(document).ready(function () {
    $("form").submit(function (event) {

        var password = $("#motdepasse").val();
        var confirmpassword = $("#confirmation").val();

        if(password != confirmpassword){
            console.log("error");
        };

        var formData = {
            name: $("#nom").val(),
            surname: $("#surnom").val(),
            password: $("#confirmation").val(),
            money: 1500,
        };
        console.log(formData);
  
      $.ajax({
        type: "POST",
        url: "http://localhost:8080/users/",
        data: JSON.stringify(formData),
        processData: false,
        contentType: 'application/json',
      }).done(function (data) {
        setCookie("id",data.id,1)
        setCookie("name",data.name,1)
        window.location.replace("http://localhost/sellcard.html")
      });
  
      event.preventDefault();
    });
  });
  

  function setCookie(cname,cvalue,exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
