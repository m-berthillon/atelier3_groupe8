$(document).ready(function () {
    $("form").submit(function (event) {

        var formData = {
            name: $("#surnom").val(),
            password: $("#motdepasse").val(),
        };
        console.log(formData);

      $.ajax({
        type: "POST",
        url: "http://localhost:8080/login",
        data: JSON.stringify(formData),
        processData: false,
        contentType: 'application/json',
      }).done(function (data) {
        setCookie("id",data.id,1)
        setCookie("name",data.id,1)
        window.location.replace("sellcard")
      });
  
      event.preventDefault();
    });
  });
  

  function setCookie(cname,cvalue,exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }