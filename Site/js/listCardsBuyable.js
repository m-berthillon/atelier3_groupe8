$(document).ready(function () {
    const userId = getCookie("id");
    var money = document.getElementById("money");
    var pseudo = document.getElementById("pseudo");
    const user = getUser(userId).then(user => {
        money.innerHTML = user.money + "€";
        pseudo.innerHTML = user.name;
    })
    $.ajax({
      url: "http://localhost:8082/market/showBuyerCards/2",
      // url: "http://localhost:8081/cards/sellable",
      dataType: "json",
      success: function(data) {

        const tableBody = $('#tableau tbody');
        console.log(data);
        $.each(data, function(index, entry) {
          const row = $('<tr></tr>');
          row.append($('<td></td>').text(entry.id));
          row.append($('<td></td>').text(entry.name));
          row.append($('<td></td>').text(entry.description));
          row.append($('<td></td>').text(entry.family));
          row.append($('<td></td>').text(entry.affinity));
          row.append($('<td></td>').text(entry.energy));
          row.append($('<td></td>').text(entry.hp));
          row.append($('<td></td>').text(entry.price + "€"));
          const button = $('<button>Acheter</button>');
          button.click(function() {
            buycard(entry.id,userId);
          });
          row.append($('<td></td>').append(button));
          tableBody.append(row);
        });

      }
  });
});

function getUser(id) {
  return new Promise(function(resolve, reject) {
      $.ajax({
          type: "GET",
          url: "user/user/1",
          dataType: "json",
          success: function(data) {
              resolve(data);
          },
          error: function(jqXHR, textStatus, errorThrown) {
              reject(errorThrown);
          }
      });
  });
}


function buycard(id,userId){
  $.ajax({
      type: "POST",
      url: "http://localhost:8082/market/buy/"+id+"/law/law",
      dataType: "json",
      success: function(data) {
        location.reload();       
      }
  });
}

function getCookie(name) {
  const cookieValue = document.cookie.match('(^|[^;]+)\\s*' + name + '\\s*=\\s*([^;]+)');
  return cookieValue ? cookieValue.pop() : '';
}