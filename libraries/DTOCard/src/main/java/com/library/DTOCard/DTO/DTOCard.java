package com.library.DTOCard.DTO;


public class DTOCard {
	
	private Integer Id;
	private String Name;
	private String Description;
	private String Family;
	private String Affinity;
	private int Energy;
	private int Hp;
	private int Price;
	private boolean Sell;
	private String Image;
	private int Owner;
	
	public int getOwner() {
		return Owner;
	}

	public void setOwner(int owner) {
		Owner = owner;
	}

	public DTOCard() {}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getFamily() {
		return Family;
	}

	public void setFamily(String family) {
		Family = family;
	}

	public String getAffinity() {
		return Affinity;
	}

	public void setAffinity(String affinity) {
		Affinity = affinity;
	}

	public int getEnergy() {
		return Energy;
	}

	public void setEnergy(int energy) {
		Energy = energy;
	}

	public int getHp() {
		return Hp;
	}

	public void setHp(int hp) {
		Hp = hp;
	}

	public int getPrice() {
		return Price;
	}

	public void setPrice(int price) {
		Price = price;
	}

	public boolean isSell() {
		return Sell;
	}

	public void setSell(boolean sell) {
		Sell = sell;
	}

	public String getImage() {
		return Image;
	}

	public void setImage(String image) {
		Image = image;
	}

}

