package com.library.DTOMarket.DTO;

import java.util.Date;

public class DTOMarket{
	
	 private Integer Id;
	 private String Type;
	 private Date Date;
	 private Integer CardId;
	 private Integer UserId;
	
	
	 public Integer getId() {
			return Id;
		}

		public void setId(Integer id) {
			Id = id;
		}
		
		
	public String getType() {
			return Type;
		}

	public void setId(String type) {
			Type = Type;
		}
	
	public Date getDate() {
		return Date;
	}

	public void setDate(Date Date) {
		Date = Date;
	}
	
	public Integer getCardId() {
		return CardId;
	}

	public void setCardId(Integer cardId) {
		CardId = cardId;
	}
	
	public Integer getUserId() {
		return UserId;
	}

	public void setUserId(Integer userId) {
		UserId = userId;
	}
}

