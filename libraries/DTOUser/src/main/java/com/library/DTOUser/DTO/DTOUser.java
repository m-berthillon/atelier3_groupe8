package com.library.DTOUser.DTO;

import java.util.List;

public class DTOUser {
	
	private int id;
    private String name;
    private String username;
    private int money;
    private String role;
    private List<Integer> cardID;
    private String password;
    
    public DTOUser(int id, String name, String username, String password, int money, String role, List<Integer> cardID) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.money = money;
        this.role = role;
        this.cardID = cardID;
        this.password = password;
    }
    
    public DTOUser() {};

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List<Integer> getCardId() {
		return cardID;
	}

	public void setCardId(List<Integer> cardId) {
		cardID = cardId;
	}
	
	
}

