package com.service.cards.service;

import com.library.DTOCard.DTO.DTOCard;
import com.service.cards.entity.Card;
import com.service.cards.repository.RepoCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ServiceCardTest {

    @Mock
    private RepoCard cRepo;

    @InjectMocks
    private ServiceCard serviceCard;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getListCards() {

        List<Integer> inputIds = Arrays.asList(1, 2, 3);
        List<Card> mockCards = Arrays.asList(
                new Card(1, "Card1", "Description1", "Family1", "Affinity1", 100, 50, 10, true,"" ),
                new Card(2, "Card2", "Description2", "Family2", "Affinity2", 150, 60, 12, true, ""),
                new Card(3, "Card3", "Description3", "Family3", "Affinity3", 200, 70, 15, true, "")
        );
        when(cRepo.findAllById(inputIds)).thenReturn(mockCards);

        // Act
        List<Integer> result = serviceCard.getListCards(inputIds);

        // Assert
        assertEquals(Arrays.asList(1, 2, 3), result);

    }

    @Test
    public void testGetCardsNotSellable() {
        // Arrange
        List<Card> mockCards = Arrays.asList(
                new Card(1, "Card1", "Description1", "Family1", "Affinity1", 100, 50, 10, false,"image" ),
                new Card(2, "Card2", "Description2", "Family2", "Affinity2", 150, 60, 12, false,"image" )
        );
        when(cRepo.findAllNotSellable()).thenReturn(mockCards);

        // Act
        List<DTOCard> result = serviceCard.getCardsNotSellable();

        // Assert
        assertEquals(2, result.size());
        assertEquals("Card1", result.get(0).getName());
        assertEquals("Card2", result.get(1).getName());

    }

    @Test
    public void testUpdateSell() {
        // Arrange
        int cardId = 1;
        int isSell = 1;
        Card mockCard = new Card(1, "Card1", "Description1", "Family1", "Affinity1", 100, 50, 10,true ,"Image1" );
        when(cRepo.findById(cardId)).thenReturn(Optional.of(mockCard));

        // Act
        DTOCard result = serviceCard.updateSell(cardId, isSell);

        // Assert
        assertEquals(false, result.isSell());
    }

    @Test
    public void testCreateDTO() {
        Card card = new Card(1, "Card1", "Description1", "Family1", "Affinity1", 100, 50, 10,true ,"Image1" );

        DTOCard dtoCard = serviceCard.createDTO(card);

        // Assert
        assertEquals(1, dtoCard.getId());
        assertEquals("Card1", dtoCard.getName());
        assertEquals("Description1", dtoCard.getDescription());

    }
    @Test
    public void testGetCardById() {
        // Arrange
        int cardId = 1;
        Card mockCard = new Card(1, "Card1", "Description1", "Family1", "Affinity1", 100, 50, 10,true ,"Image1" );
        when(cRepo.findCardById(cardId)).thenReturn(mockCard);

        // Act
        DTOCard result = serviceCard.getCardById(cardId);

        // Assert
        assertEquals("Card1", result.getName());
        assertEquals("Description1", result.getDescription());
        // Similarly, assert other properties
    }

    @Test
    public void testGetCardsSellable() {
        // Arrange
        List<Card> mockCards = Arrays.asList(
                new Card(1, "Card1", "Description1", "Family1", "Affinity1", 100, 50, 10, true, "Image"),
                new Card(2, "Card2", "Description2", "Family2", "Affinity2", 150, 60, 12, true, "Images")
        );
        when(cRepo.findAllSellable()).thenReturn(mockCards);

        // Act
        List<DTOCard> result = serviceCard.getCardsSellable();

        // Assert
        assertEquals(2, result.size());
        assertEquals("Card1", result.get(0).getName());
        assertEquals("Card2", result.get(1).getName());

    }
}