package utils;

import java.util.List;

public class CardIdsRequest {
    private List<Integer> cardIds;

    public List<Integer> getCardIds() {
        return cardIds;
    }

    public void setCardIds(List<Integer> cardIds) {
        this.cardIds = cardIds;
    }
}
