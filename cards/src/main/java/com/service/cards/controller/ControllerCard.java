package com.service.cards.controller;

import java.util.List;

import com.library.DTOCard.DTO.DTOCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.service.cards.entity.Card;
import com.service.cards.service.ServiceCard;

import utils.CardIdsRequest;

@RestController
public class ControllerCard {
	@Autowired
	ServiceCard serviceCard;
	
	@GetMapping("/cards/{id}")
	public DTOCard findById(@PathVariable int id) {
	    return serviceCard.getCardById(id);
	}
	
	@PostMapping("/cards")
	public Iterable<Integer> findAllSellable(@RequestBody CardIdsRequest request) {
	    return serviceCard.getListCards(request.getCardIds());
	}
	
	@GetMapping("/cards/sellable")
	public List<DTOCard> getCardsSellable() {
	    return serviceCard.getCardsSellable();
	}

	@GetMapping("/cards/all")
	public List<DTOCard> getAllCards() {
		return serviceCard.getAllCards();
	}
	
	@GetMapping("/cards/notsellable")
	public List<DTOCard> getCardsNotSellable() {
	    return serviceCard.getCardsNotSellable();
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/cards/{id}/{isSell}")
	public DTOCard updateIsSell(@PathVariable int id, @PathVariable int isSell) {
		return serviceCard.updateSell(id,isSell);
	}


	
}
