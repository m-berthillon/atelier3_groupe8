package com.service.cards.service;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.DTOCard.DTO.DTOCard;
import com.service.cards.entity.Card;
import com.service.cards.repository.RepoCard;


@Service
public class ServiceCard {
	@Autowired
	RepoCard cRepo;
	

	public List<Integer> getListCards(List<Integer> listIdCard) {
	    Iterable<Integer> iterable = listIdCard;
	    List<Card> cards =  (List<Card>) cRepo.findAllById(iterable);
	    List<Integer> idCards = new ArrayList<>();
	    for (Card card : cards) {
	    	idCards.add(card.getId());
		}
	    return idCards;
	}
	public List<DTOCard> getAllCards(){
		return cRepo.findAll().stream().map(this::createDTO).collect(Collectors.toList());
	}
	
	public DTOCard getCardById(int id)
	{
		Card card = cRepo.findCardById(id);
		
		return createDTO(card);
	}
	
	public List<DTOCard> getCardsSellable()
	{
		
		List<Card>  cards = (List<Card>) cRepo.findAllSellable();
		List<DTOCard> dtoCards = new ArrayList<>();
		for (Card card : cards) {
			
			dtoCards.add(createDTO(card));
		}
		return dtoCards;
	}
	
	public List<DTOCard> getCardsNotSellable()
	{
		
		List<Card>  cards = (List<Card>) cRepo.findAllNotSellable();
		List<DTOCard> dtoCards = new ArrayList<>();
		for (Card card : cards) {
			
			dtoCards.add(createDTO(card));
		}
		return dtoCards;
	}
	
	public DTOCard updateSell(int id,int isSell) {
	    Card card = cRepo.findById(id).orElse(null);
	    if (card != null) {
	    	if(isSell == 0) {
	    		card.setSell(false);
	    	}
	    	if(isSell == 1) {
	    		card.setSell(true);
	    	}
	    	
	    	cRepo.save(card);
	    }
		return createDTO(card);
	}



	
	public DTOCard createDTO (Card  card)
	{
		DTOCard  dtoCard = new DTOCard();
		dtoCard.setId(card.getId());
		dtoCard.setName(card.getName());
		dtoCard.setDescription(card.getDescription());
		dtoCard.setFamily(card.getFamily());
		dtoCard.setAffinity(card.getAffinity());
		dtoCard.setEnergy(card.getEnergy());
		dtoCard.setHp(card.getHp());
		dtoCard.setPrice(card.getPrice());
		dtoCard.setImage(card.getImage());
		return dtoCard ;
	}
		

}

