package com.service.cards.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.service.cards.entity.Card;

public interface RepoCard extends JpaRepository<Card, Integer>
{	
	
	@Query("select c from Card c where c.Sell = true")
	Iterable<Card> findAllSellable();
	
	@Query("select c from Card c where c.Sell = false ")
	Iterable<Card> findAllNotSellable();
	
	@Query("select c from Card c where c.Id = :id")
	Card findCardById(@Param("id") int id); 
	
	

}
